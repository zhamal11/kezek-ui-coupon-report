Highcharts.chart('container2', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
            '<5',
            '5-10',
            '10-15',
            '15-30',
            '>30'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Время ожидания',
        data: [49, 71, 106, 129, 144],
        color: '#F6511D'

    }, {
        name: 'Время обслуживания',
        data: [83, 78, 98, 93, 106],
        color: '#00A6ED'

    }]
});

Highcharts.chart('container3', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
            'Обслужено',
            'Перенаправлен',
            'Отложено'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Время ожидания',
        data: [409, 71, 106],
        color: '#14B668'

    }]
});

Highcharts.chart('container', {
    chart: {
        type: 'variablepie'
    },
    title: {
        text: ''
    },
    tooltip: {
        headerFormat: '',
        pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
            'Area (square km): <b>{point.y}</b><br/>' +
            'Population density (people per square km): <b>{point.z}</b><br/>'
    },
    series: [{
        minPointSize: 10,
        innerSize: '50%',
        zMin: 0,
        name: 'countries',
        data: [{
            name: 'Без бронирования',
            y: 505370,
            z: 92.9,
            color: '#F6511D'
        }, {
            name: 'Бронирование',
            y: 551500,
            z: 118.7,
            color: '#14B668'
        }],
        dataLabels: {
            enabled: false
        },
        showInLegend: true,
    }]
});

